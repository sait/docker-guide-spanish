### Servicio Docker
```
service docker status
service docker start
```

### Primeros pasos
```
docker
docker info
docker version
docker images
```


### Crear Contenedor
```
docker run --detach --name=mycontainer --env="MYSQL_ROOT_PASSWORD=secret" mariadb:10
```


### Operaciones con Contenedores
```
docker ps
docker ps -a
docker stop mycontainer
docker start mycontainer
docker restart mycontainer
docker inspect mycontainer
docker inspect mycontainer | grep IPAddress
docker logs mycontainer
docker logs -f mycontainer
```


### Borrar contenedor
```
docker stop mycontainer
docker rm mycontainer
```


### Ejecutar comandos EN el contenedor
```
docker exec mycontainer ls -l
docker exec mycontainer date
docker exec mycontainer cp /usr/share/zoneinfo/America/Phoenix /etc/localtime

-- Mejor entrar al contenedor mediante bash
docker exec -it mycontainer bash
```

