### Subir una imagen a nuestro Docker Registry
```
--- Crear un contenedor
docker run --name=mycontainer -ti ubuntu /bin/bash

--- Crear un archivo texto en /
echo "Hola Mundo Cruel" > /hello.txt

--- Salir del contenedor
exit

--- Entrar nuevamente al contenedor en modo interactivo
docker start mycontainer -i
ll /
cat /hello.txt
exit

--- Crear imagen local basada en contenedor
docker commit mycontainer myimage

--- Ver imagenes descargadas o creadas
docker images

--- Firmarse en el DockerRegistry
docker login -u myUsername -p mySecretPassword  myDockerRegistryDomain.com
docker login -u myUsername -p mySecretPassword  dockerhub.sait.mx

--- Etiquetar la imagen con nuestro DockerRegistry y darle push
docker tag myimage dockerhub.sait.mx/myimage
docker push dockerhub.sait.mx/myimage

```


### Crear un contenedor usando una imagen de nuestro DockerRegistry
```
--- Firmarse a nuestro DockerRegistry
docker login -u myUsername -p mySecretPassword  dockerhub.sait.mx

--- Crear  un contenedor en base a nuestra imagen almacenada en nuestro DockerRegistry
docker run --detach --name=mycontainer dockerhub.sait.mx/myimage 

--- Acceder al contenedor para verificar
docker exec -it mycontainer bash

--- Verificar que nuestro archivo de prueba, existe en el contenedor
cat /hello.txt

--- Salir del contenedor
exit


```